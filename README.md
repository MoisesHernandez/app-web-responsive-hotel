# Caso de estudio 3: Aplicación web responsiva de una cadena hotelera (Hotel Paradise)
### Seminario de solución de problemas de ingeniería de software I
***Maestro:*** Luis Antonio Medellín Serna

***Alumno:*** Moisés Hernández Del toro

***Código:*** 214583939

1.- Primero necesitamos clonar el proyecto

2.- Luego Instalamos XAMPP y introducimos dentro de hdocs este proyecto.

3.- Necesitamos importar la base de datos en nuestro phpMyAdmin que se encuentra en la carpeta de base de datos.

4.- Con lo anterior realizado podremos utilizar la aplicación abriendo cualquier navegador y ejecutando localhost.

6.- Para acceder como administrador necesitamos introducir la siguiente información en la pestaña de "ingresar".

Correo: admin@admin.com

Contraceña: admin

___

Capturas

![Captura1](Capturas/Captura1.png)

![Captura2](Capturas/Captura2.png)

![Captura3](Capturas/Captura3.png)



